#include "node.h"

//namespace GMlib {

Node::Node(): _v(){
}

Node::Node(GMlib::TSVertex<float>* v_in):_v(v_in){}

GMlib::Array<GMlib::TSTriangle <float>* > Node::getTriangles()
{
  return _v->getTriangles();
}

GMlib::TSEdge<float>* Node::getNeighbor(Node& n)
{
  GMlib::Array<GMlib::TSEdge<float>*> edges = _v->getEdges();
  for (auto i=0;i<edges.size();i++)
  {
    if ( edges[i]->getOtherVertex(*_v)==n.getV())
      return edges[i];
  }
  return nullptr;
}

void Node::setZ(float z)
{
    _v->setZ(z);
}

GMlib::TSVertex<float>* Node::getV()
{
  return _v;
}

//}
