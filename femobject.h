#ifndef FEMOBJECT_H
#define FEMOBJECT_H

#include "node.h"

class FEMObject: public GMlib::TriangleFacets<float>
{
  GMlib::ArrayLX<Node> _nodes;
  GMlib::DMatrix<float> _A;
  GMlib::DVector<float> _b;
  int _n_bound;
  float _f;
  double _force_simulate=0.0;
public:
  FEMObject();
  void regularTriangulation(int n, int m, float r);
  void randomTriangulation(int m, float r);
  void Computation();
  void UpdateHeight(double f);
  GMlib::Vector<GMlib::Vector<float,2>,3> findVectors(GMlib::TSEdge<float>* ed, GMlib::TSVertex<float>* in_v);
  GMlib::Vector<GMlib::Vector<float,2>,3> findVectorsDiagonal(GMlib::TSTriangle<float>* triangle, GMlib::TSVertex<float>* in_v);
  void setInitForce(double in_force);
protected:
  void localSimulate(double dt) override;
};

#endif // FEMOBJECT_H
