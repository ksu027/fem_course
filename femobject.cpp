#include "femobject.h"
#include <random>

FEMObject::FEMObject(){}

void FEMObject::regularTriangulation(int n, int m, float r)
{
  auto n_new=n;
  this->insertAlways(GMlib::TSVertex<float>(GMlib::Point<float,2> (0.0,0.0)));
  for (auto j=1 ; j<=m; ++j)
  {
    for (auto i=0 ; i<n_new; ++i)
    {
      auto alpha = i*M_2PI/n_new;
      GMlib::SqMatrix<float,2> R (alpha,GMlib::Vector<float,2>(1,0),GMlib::Vector<float,2>(0,1));
      GMlib::Point<float,2> p = R*GMlib::Point<float,2>(j*r/m,0);
      this->insertAlways(GMlib::TSVertex<float>(p));
    }
    n_new=n*(j+1);
  }


  const auto test_size = this->size();

  this->reverse();
  this->_n_bound=n*m;

};

void FEMObject::randomTriangulation(int m, float r)
{
  const auto node_size  = 5.0;
  const auto swap_order = 3;
  const auto del_percent = 0.7;
  auto levels = int(ceil(std::abs(m/node_size)));
  if (levels<20) levels=20;
  regularTriangulation(int(node_size),levels,r);

  /* set up randomizer */
  std::random_device rd;
  std::mt19937 gen(rd());
  //std::uniform_int_distribution<> random_n(_n_bound,this->size()-1);
  std::uniform_int_distribution<> random_1st_half(_n_bound,int(ceil((this->size()-1)/2)));
  std::uniform_int_distribution<> random_2nd_half(int(ceil((this->size()-1)/2)),this->size()-1);

  auto swaps = std::pow(_n_bound,swap_order);
  auto del_num = int(std::ceil((size() - _n_bound) * del_percent));

  /* swaping */
  for (auto i = 0; i < swaps; ++i)
  {
    this->swap(random_1st_half(gen),random_2nd_half(gen));
  }


  /* deleting */
  for (auto i = 0; i < del_num; ++i)
    this->removeBack();

};

void FEMObject::Computation()
{
  /* find nodes */
  _nodes.clear();
  const auto n_vert = this->getNoVertices();
  for (auto i=(this->_n_bound); i<n_vert; ++i)
  {
    this->_nodes+= Node(this->getVertex(i));
  }

  /* inti A to zero */
  const auto n_nodes = _nodes.size();
  _A.setDim(n_nodes,n_nodes);
  _b.setDim(n_nodes);
  for (auto i=0;i<n_nodes;++i)
  {
    for (auto j=0;j<n_nodes;++j)
    {
      _A[i][j]=0.0f;
    }
  }


  for (auto i = 0; i<n_nodes;++i)
  {
    for (auto j = 0; j<i; ++j)
    {
      GMlib::TSEdge<float>* edge = _nodes[i].getNeighbor(_nodes[j]);
      if (edge)
      {
        /* compute non-diagonal element of the stiffness matrix */
        GMlib::Vector<GMlib::Vector<float,2>,3> count_vectors = findVectors(edge,_nodes[i].getV());

        const GMlib::Vector<float,2> d  = count_vectors[0];
        const GMlib::Vector<float,2> a1 = count_vectors[1];
        const GMlib::Vector<float,2> a2 = count_vectors[2];

        const auto dd    = 1.0f/(std::abs(d*d));
        const auto dh1   = dd*(a1*d);
        const auto dh2   = dd*(a2*d);
        const auto area1 = std::abs(d^a1);
        const auto area2 = std::abs(d^a2);
        const auto h1    = dd*pow(area1,2);
        const auto h2    = dd*pow(area2,2);

        _A[i][j]=_A[j][i]=(dh1*(1-dh1)/h1-dd)*(area1/2) +
                          (dh2*(1-dh2)/h2-dd)*(area2/2);
      }
    }

    /* compute diagonal element of stiffness matrix */
    GMlib::Array<GMlib::TSTriangle<float>*> triangles = _nodes[i].getTriangles();
    float Tk_sum = 0.0f;
    float node_area = 0.0f;
    for ( auto j = 0; j<triangles.size(); ++j)
    {
      GMlib::Vector<GMlib::Vector<float,2>,3> vec_for_comp = findVectorsDiagonal(triangles[j],_nodes[i].getV());
      const auto Tk = (vec_for_comp[2]*vec_for_comp[2])/(2*std::abs(vec_for_comp[0]^vec_for_comp[1]));
      Tk_sum+=Tk;
      node_area+=triangles[j]->getArea2D();
    }
    _A[i][i]=Tk_sum;


    /* compute the load vector */
    _b[i]=node_area/3.0f;
  }
  _A.invert();
}

void FEMObject::UpdateHeight(double f)
{
    GMlib::DVector<float> x=_A*(f*_b);

    for (auto i = 0; i<_nodes.size();++i)
    {
      _nodes[i].setZ(x[i]);
    }

}

GMlib::Vector<GMlib::Vector<float,2>,3> FEMObject::findVectors(GMlib::TSEdge<float>* ed, GMlib::TSVertex<float>* in_v)
{
  const auto other_vertex = ed->getOtherVertex(*in_v);
  GMlib::Vector<float,2> d = other_vertex->getParameter()-in_v->getParameter();

  GMlib::Array<GMlib::TSTriangle<float>*> T = ed->getTriangle();
  GMlib::Array<GMlib::TSVertex<float>*> V1 = T[0]->getVertices();
  GMlib::Array<GMlib::TSVertex<float>*> V2 = T[1]->getVertices();

  GMlib::Vector<float,2> a1;
  GMlib::Vector<float,2> a2;

  for (int var = 0; var < V1.size(); ++var) {
    if ((V1[var]!=in_v) and (V1[var]!=other_vertex))
    {
      a1=V1[var]->getParameter()-in_v->getParameter();
    }
    if ((V2[var]!=in_v) and (V2[var]!=other_vertex))
    {
      a2=V2[var]->getParameter()-in_v->getParameter();
    }
  }
  return GMlib::Vector<GMlib::Vector<float,2>,3> (d,a1,a2);
}

GMlib::Vector<GMlib::Vector<float,2>,3> FEMObject::findVectorsDiagonal(GMlib::TSTriangle<float>* triangle, GMlib::TSVertex<float>* in_v)
{
  GMlib::Array<GMlib::TSVertex<float>*> vertecies = triangle->getVertices();
  GMlib::Vector<float,2> d1;
  GMlib::Vector<float,2> d2;
  GMlib::Vector<float,2> d3;
  GMlib::Point<float,2> p1;
  GMlib::Point<float,2> p2;
  bool get_in = true;
  for (int i = 0; i < vertecies.size(); ++i)
  {
    if (vertecies[i]!=in_v and get_in )
    {
      d1=vertecies[i]->getParameter()-in_v->getParameter();
      p1=vertecies[i]->getParameter();
      get_in=false;
    } else if (vertecies[i]!=in_v) {
        d2=vertecies[i]->getParameter()-in_v->getParameter();
        p2=vertecies[i]->getParameter();
        d3=p2-p1;
    }
  }
  return GMlib::Vector<GMlib::Vector<float,2>,3> (d1,d2,d3);
}

void FEMObject::setInitForce(double in_force)
{
  _force_simulate = in_force;
}

void FEMObject::localSimulate(double dt)
{
  _force_simulate += dt;
  UpdateHeight(0.5*std::sin(_force_simulate));
}
