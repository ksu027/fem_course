#ifndef NODE_H
#define NODE_H


#include <trianglesystem/gmtrianglesystem.h>
//#include <core/containers/gmarray.h>


//namespace GMlib {
  class Node
  {
    GMlib::TSVertex<float>* _v;
    public:
      Node();
      Node(GMlib::TSVertex<float>* v_in);
      GMlib::Array<GMlib::TSTriangle <float>* > getTriangles();
      GMlib::TSEdge<float>* getNeighbor(Node& n);
      void setZ(float z);
      GMlib::TSVertex<float>* getV();
  };
//};
#endif // NODE_H
